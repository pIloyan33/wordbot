import { TelegrafModule } from 'nestjs-telegraf';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { sessionMiddleware } from './middlewares/session.middleware';

@Module({
  imports: [
    ConfigModule,
    TelegrafModule.forRoot({
      botName: 'randomWordsListBot',
      token: '5762022258:AAEh2cpt0-OCEIC2oGdO3Zf04_75fEoidKU',
      middlewares: [sessionMiddleware],
    }),
  ],

  exports: [TelegrafModule],
})
export class TelegramModule {}
