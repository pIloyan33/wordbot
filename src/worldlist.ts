export const f = [
  {
    name: '(at the) bus stop',
  },
  {
    name: 'public transport',
  },
  {
    name: 'validate (your ticket)',
  },
  {
    name: 'escalator',
  },
  {
    name: 'underground',
  },
  {
    name: 'underpass',
  },
  {
    name: 'metro',
  },
  {
    name: 'subway',
  },
  {
    name: 'bridge',
  },
  {
    name: 'fountain',
  },
  {
    name: 'cross',
  },
  {
    name: 'wait (for)',
  },
  {
    name: 'turn left/right',
  },
  {
    name: 'go by _____',
  },
  {
    name: 'take a (taxi)',
  },
  {
    name: 'go upstairs',
  },
  {
    name: 'go downstairs',
  },
  {
    name: 'get on/off',
  },
  {
    name: 'close to',
  },
  {
    name: 'not far from',
  },
  {
    name: '',
  },
  {
    name: '(in/on the) street',
  },
  {
    name: 'go along the street',
  },
  {
    name: '(on the) road',
  },
  {
    name: '(on the) sidewalk',
  },
  {
    name: '(take a) crosswalk',
  },
  {
    name: '(light/heavy) traffic',
  },
  {
    name: 'traffic lights',
  },
  {
    name: '(be in a) traffic jam',
  },
  {
    name: '(at the) crossroads',
  },
  {
    name: 'building',
  },
  {
    name: 'block of flats',
  },
  {
    name: '(in the) playground',
  },
  {
    name: '(at the) parking',
  },
  {
    name: '(in the) center',
  },
  {
    name: 'go / went',
  },
  {
    name: 'walk',
  },
  {
    name: 'drive',
  },
  {
    name: 'jump',
  },
  {
    name: 'up (the street)',
  },
  {
    name: 'down (the street)',
  },
  {
    name: 'past (the shop)',
  },
  {
    name: 'through (the park)',
  },
  {
    name: 'over (the river), (jump over the fire), (fly over London)',
  },
  {
    name: 'under (the bridge)',
  },
  {
    name: 'across (the square)',
  },
  {
    name: 'go across the river/ocean',
  },
  {
    name: 'along (the road)',
  },
  {
    name: 'around (the building)',
  },
  {
    name: 'towards (the museum)',
  },
  {
    name: 'ahead ',
  },
  {
    name: 'straight (along this street)',
  },
  {
    name: 'close',
  },
  {
    name: 'cook ',
  },
  {
    name: 'deliver',
  },
  {
    name: 'write',
  },
  {
    name: 'read',
  },
  {
    name: 'pay',
  },
  {
    name: 'send',
  },
  {
    name: 'break',
  },
  {
    name: 'eat',
  },
  {
    name: 'drink',
  },
  {
    name: 'cut',
  },
  {
    name: 'scarf',
  },
  {
    name: 'glove(s)',
  },
  {
    name: '(bow) tie',
  },
  {
    name: 'glasses',
  },
  {
    name: 'sunglasses',
  },
  {
    name: 'jewelry',
  },
  {
    name: '(diamond) ring',
  },
  {
    name: 'precious stone',
  },
  {
    name: 'chain',
  },
  {
    name: 'bracelet',
  },
  {
    name: 'earrings',
  },
  {
    name: 'bra',
  },
  {
    name: 'panties',
  },
  {
    name: 'briefs',
  },
  {
    name: 'tights',
  },
  {
    name: '(hand)bag',
  },
  {
    name: 'purse',
  },
  {
    name: 'wallet',
  },
  {
    name: 'knockoff',
  },
  {
    name: 'boots',
  },
  {
    name: 'high-heeled shoes',
  },
  {
    name: 'flat shoes',
  },
  {
    name: 'slippers',
  },
  {
    name: 'sandals',
  },
  {
    name: 'sneakers/ trainers',
  },
  {
    name: 'button',
  },
  {
    name: 'zipper',
  },
  {
    name: 'shoelace',
  },
  {
    name: 'wear (wore)',
  },
  {
    name: 'put on (put on)',
  },
  {
    name: 'take off (took off)',
  },
  {
    name: 'try on',
  },
  {
    name: 'do up',
  },
  {
    name: 'tight',
  },
  {
    name: 'loose',
  },
  {
    name: 'suit',
  },
  {
    name: 'fit',
  },
  {
    name: 'fitting room',
  },
  {
    name: '(air)plane',
  },
  {
    name: 'terminal',
  },
  {
    name: 'departures',
  },
  {
    name: 'flight',
  },
  {
    name: 'check in',
  },
  {
    name: 'boarding pass',
  },
  {
    name: '(go through) customs control',
  },
  {
    name: '(go through) passport    control',
  },
  {
    name: 'departure lounge',
  },
  {
    name: 'duty free (shop)',
  },
  {
    name: 'gate',
  },
  {
    name: 'board the plane',
  },
  {
    name: 'boarding',
  },
  {
    name: 'fasten seat belts',
  },
  {
    name: 'take off (verb/noun)',
  },
  {
    name: 'go by plane',
  },
  {
    name: 'land',
  },
  {
    name: 'landing',
  },
  {
    name: 'crew',
  },
  {
    name: 'flight attendant',
  },
  {
    name: 'pilot',
  },
  {
    name: 'captain',
  },
  {
    name: 'arrivals',
  },
  {
    name: 'baggage / luggage',
  },
  {
    name: 'baggage claim',
  },
  {
    name: 'carousel',
  },
  {
    name: 'suitcase',
  },
  {
    name: 'weigh',
  },
  {
    name: 'extra weight',
  },
  {
    name: '(have a) direct flight',
  },
  {
    name: 'connecting flight',
  },
  {
    name: 'domestic',
  },
  {
    name: 'international',
  },
  {
    name: 'carry-on',
  },
  {
    name: 'hand luggage',
  },
  {
    name: 'delay  ',
  },
  {
    name: 'cancel ',
  },
  {
    name: 'lost-and-found office ',
  },
  {
    name: 'mix up ',
  },
  {
    name: 'refund expenses ',
  },
  {
    name: '(fly with/ low cost) airlines ',
  },
  {
    name: '(take a) shuttle',
  },
  {
    name: 'go shopping',
  },
  {
    name: 'goods',
  },
  {
    name: '(buy at a high/low) price',
  },
  {
    name: 'cheap',
  },
  {
    name: 'expensive',
  },
  {
    name: 'free',
  },
  {
    name: '(buy sth on) sale',
  },
  {
    name: '(at a) discount',
  },
  {
    name: '(under) guarantee',
  },
  {
    name: 'bargain (verb)',
  },
  {
    name: 'cash desk',
  },
  {
    name: 'shop assistant',
  },
  {
    name: '(can) afford (to buy sth) ',
  },
  {
    name: 'How much does it cost?',
  },
  {
    name: 'How much is it?',
  },
  {
    name: 'mall',
  },
  {
    name: 'free shipping',
  },
  {
    name: 'expiry date',
  },
  {
    name: 'change currency',
  },
  {
    name: 'pay for',
  },
  {
    name: '(of _low_) quality',
  },
  {
    name: 'pay (in) cash',
  },
  {
    name: 'pay by card',
  },
  {
    name: 'borrow (from)',
  },
  {
    name: 'lend (sth to sb)',
  },
  {
    name: 'save (for)',
  },
  {
    name: 'spend (on)',
  },
  {
    name: 'waste (on)',
  },
  {
    name: 'earn',
  },
  {
    name: 'salary',
  },
  {
    name: 'ATM',
  },
  {
    name: '(be in) debt (to sb)',
  },
  {
    name: 'enough',
  },
  {
    name: 'broke',
  },
  {
    name: 'well off',
  },
  {
    name: 'open a bank account',
  },
];

// ('person'),('people'),('relatives'),('family'),('in-laws'),('colleague'),('classmate'),('superior'),('subordinate'),('neighbor'),('stranger'),('foreigner'),('buddy'),('enemy'),('passer-by'),('kind'),('generous'),('friendly'),('cheerful'),('shy'),('reliable'),('unreliable'),('cowardly'),('lovely'),('strict'),('talkative'),('mean'),('selfish'),('cold'),('naughty'),('diligent'),('witty'),('calm'),('lucky'),('caring'),('be afraid (of)'),('be angry (with)'),('be worried (about)'),('be glad (about)'),('be happy (for)'),('be sure (about)'),('be proud (of)'),('be sorry (for)'),('be late (for)'),('camp '),('camper'),('outdoor  (swimming pool)'),('(sleep, play) outdoors'),('(go on a) journey / trip'),('hike'),('hitchhike'),('backpack'),('sleeping bag'),('(go by) boat'),('moist wipe'),('bug spray'),('flashlight'),('lake'),('(go to the) river'),('play the guitar'),('play board games'),('play volleyball'),('have a barbecue'),('grill (vegetables / meat)'),('spend time'),('climb the mountains'),('put up a tent'),('take down a tent'),('ride a bicycle (a horse)'),('in the open air'),('(cut with a) Swiss army knife'),('matches'),('make a fire'),('put out a /fire'),('(go to the) forest'),('(go on a) picnic')
