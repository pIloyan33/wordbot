import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';
import { DatabaseModule } from './database.module';
import { BotModule } from './module/bot/bot.module';
import { TelegramModule } from './telegram.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.env`,
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DB: Joi.string().required(),
        PORT: Joi.number(),
        TOKEN_BOT_SECURE: Joi.string().required(),
        NAME_BOT: Joi.string().required(),
      }),
      isGlobal: true,
    }),
    DatabaseModule,
    TelegramModule,
    BotModule,
  ],
})
export class AppModule {}
