export class CreateProductDTO {
  name: string;
  translate?: string;
  translationBy?: string;
}
