import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDTO } from './create-product.dto';
import Product from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  async createProduct(dto: CreateProductDTO) {
    const newProduct = this.productRepository.create(dto);
    await this.productRepository.save(newProduct);
    return newProduct;
  }

  async find({ id }: any) {
    return await this.productRepository.findOne({ id });
  }

  async update(data: {
    id: number;
    translation: string;
    translationBy?: string;
  }) {
    return await this.productRepository.update(
      { id: data.id },
      { translation: data.translation, translationBy: data.translationBy },
    );
  }
}
