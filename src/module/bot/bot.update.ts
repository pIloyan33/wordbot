// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Injectable } from '@nestjs/common';
import { Context } from '../../interfaces/context.interface';
import { Ctx, On, Start, Update } from 'nestjs-telegraf';
import { BotService } from './bot.service';
import { Markup } from 'telegraf';
import { ADD_TRANSLATION } from 'src/constants/constants';

export const mainKeyboard = Markup.keyboard([
  [Markup.button.text(`🚀 Get random word`)],
]).resize();

@Injectable()
@Update()
export class BotUpdate {
  constructor(private botService: BotService) {}

  @Start()
  async onStart(ctx: Context) {
    try {
      await ctx.reply(
        `Hi, ${
          ctx?.update?.message?.from?.first_name
            ? ctx?.update?.message?.from?.first_name
            : ''
        } 😊\nPlease don't make a lot of requests at once 🙏\nOf course you can but don\`t send as crazy`,
        mainKeyboard,
      );
    } catch (e) {
      console.log(e);
    }
  }

  @On('callback_query')
  async callbackQueryCheck(@Ctx() ctx: Context) {
    try {
      this.clearMarkupEditMessage(ctx);
      const { command, answer }: { command: string; answer: any } = JSON.parse(
        //@ts-ignore
        ctx.update.callback_query.data,
      );

      if (command === '/addTranslate') {
        await ctx.scene.enter(ADD_TRANSLATION, {
          id: answer,
          by: ctx?.update?.callback_query?.from?.first_name,
        });

        return;
      }
    } catch (e) {
      console.log(e);
    }
  }

  @On('text')
  async onMessage(ctx: Context) {
    //@ts-ignore
    if (ctx.message.text === '🚀 Get random word') {
      //@ts-ignore
      console.log('got random word ', ctx?.update?.message?.from?.first_name);

      const word = await this.botService.getRandomWord();
      if (!word) {
        await ctx.reply(`😱 nothing found`, mainKeyboard);
        return;
      }

      if (!word?.translation) {
        await ctx.reply(
          word.name,
          Markup.inlineKeyboard([
            Markup.button.callback(
              'add translation',
              JSON.stringify({
                command: '/addTranslate',
                answer: word.id,
              }),
            ),
          ]),
        );
        return;
      }
      await ctx.reply(`Word ${word.name}`);
      await ctx.reply(
        `Translation: ||${word?.translation}||,\nby ${word?.translationBy}`,
        { parse_mode: 'MarkdownV2' },
      );
      return;
    }
  }

  private async clearMarkupEditMessage(ctx: Context, text?: string) {
    try {
      await ctx.editMessageReplyMarkup(null);
      if (text) {
        await ctx.editMessageText(text);
      }
    } catch (e) {
      console.log(e);
    }
  }
}
