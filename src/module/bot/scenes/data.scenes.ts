/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ADD_TRANSLATION } from '../../../constants/constants';
import { Context, Wizard, WizardStep, On } from 'nestjs-telegraf';
import { Markup, Scenes } from 'telegraf';
import { BotService } from '../bot.service';
import { mainKeyboard } from '../bot.update';

const exit = Markup.keyboard([[Markup.button.text(`❌ Exit`)]]).resize();

@Wizard(ADD_TRANSLATION)
export class CustomerData {
  constructor(private botService: BotService) {}

  @WizardStep(1)
  async step1(
    @Context()
    ctx: Scenes.WizardContext,
  ) {
    ctx.reply('put translation', exit);
    ctx.wizard.next();
    return;
  }

  @On('text')
  @WizardStep(2)
  async step3(@Context() ctx: Scenes.WizardContext) {
    try {
      //@ts-ignore
      if (ctx.message.text === '❌ Exit') {
        await ctx.reply(`Okay`, mainKeyboard);
        await ctx.scene.leave();
        return;
      }

      await this.botService.addTranslation({
        //@ts-ignore
        id: ctx.wizard.state.id,
        //@ts-ignore
        translation: ctx.message.text,
        //@ts-ignore
        translationBy: ctx.wizard.state.by,
      });
      //@ts-ignore
      const word = await this.botService.getWord(ctx.wizard.state.id);

      await ctx.reply(
        `Word: ${word.name},\nTranslation: ${word.translation},\nby ${word.translationBy}`,
        mainKeyboard,
      );
      await ctx.scene.leave();
      return;
    } catch (error) {
      ctx.wizard.step;
    }
  }
}
