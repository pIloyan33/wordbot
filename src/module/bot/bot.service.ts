/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Injectable } from '@nestjs/common';
import { ProductService } from '../product/product.service';
import Product from '../product/product.entity';

@Injectable()
export class BotService {
  constructor(private productService: ProductService) {}

  async getRandomWord(): Promise<Product> {
    const id = Math.floor(Math.random() * 255); // here bad solution
    const word: Product = await this.productService.find({ id });

    return word;
  }

  async getWord(wordId: number): Promise<Product> {
    const word: Product = await this.productService.find({ id: wordId });

    return word;
  }

  async addTranslation(data: any) {
    return await this.productService.update(data);
  }
}
