import { ProductModule } from './../product/product.module';
import { Module } from '@nestjs/common';
import { BotService } from './bot.service';
import { BotUpdate } from './bot.update';
import { Telegraf } from 'telegraf';
import { CustomerData } from './scenes/data.scenes';

@Module({
  imports: [ProductModule],
  providers: [BotService, BotUpdate, Telegraf, CustomerData],
  exports: [BotService],
})
export class BotModule {}
